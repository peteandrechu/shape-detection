/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagecapture;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JLabel;

/**
 *
 * @author JR
 */
public class ImageProcess {
    
    private File source;
    private BufferedImage image;
    private BufferedImage write_image;
    private int height, width;
    public String contour;
    private int thres = 350;
    private List<Point> points = new ArrayList<>();
    private Point center = new Point(0,0);
    private JLabel shapeLabel;
    
    public ImageProcess(String file, JLabel shapeLabel) throws IOException{
        this.source = new File(file);
        this.image = ImageIO.read(this.source);
        this.width = this.image.getWidth();
        this.height = this.image.getHeight();
        this.write_image =  new BufferedImage(width,height,this.image.getType());
        this.shapeLabel = shapeLabel;
    }
    
    public void ProcessImage() throws IOException{
        greyScale();
        convertBinary();
        findContour();
        findCorner();
    }
    
    public void greyScale() throws IOException{                
         File input = this.source;
         image = ImageIO.read(input);
         width = image.getWidth();
         height = image.getHeight();
         
         for(int i=0; i<height; i++) {
         
            for(int j=0; j<width; j++) {
            
               Color c = new Color(image.getRGB(j, i));
               int red = (int)(c.getRed() * 0.299);
               int green = (int)(c.getGreen() * 0.587);
               int blue = (int)(c.getBlue() *0.114);
               Color newColor = new Color(red+green+blue,
               
               red+green+blue,red+green+blue);
               
               image.setRGB(j,i,newColor.getRGB());
            }
         }
         
         this.source = new File("C:\\Users\\JR\\Desktop\\testGreyScale.jpg");
         File ouptut = this.source;
         ImageIO.write(image, "jpg", ouptut);
         this.image = ImageIO.read(this.source);

    }
    
    public void convertBinary() throws IOException{
        for(int i=0;i<height;i++){
             for(int j=0;j<width;j++){
                 Color c = new Color(image.getRGB(j, i));
                 int red = (int)(c.getRed());
                 int green = (int)(c.getGreen());
                 int blue = (int)(c.getBlue());
                 int total = red+green+blue;
                 Color newColor = (total>thres)? Color.white: Color.BLACK;
                 write_image.setRGB(j,i,newColor.getRGB());
                 
             }
         }
        
        this.source = new File("C:\\Users\\JR\\Desktop\\testBinary.jpg");
       
        File ouptut = this.source;
        ImageIO.write(write_image, "jpg", ouptut);
         this.image = ImageIO.read(this.source);
    }
    
    public void findContour()throws IOException{
        int count =0;
        BufferedImage white_image = ImageIO.read(this.source);
        
        
        
        for(int i = 0;i<height;i++){
            for(int j = 0;j<width;j++){
                white_image.setRGB(j,i,Color.WHITE.getRGB());
            }
        }
        
        for(int i=0;i<height;i++){
            count=0;
             for(int j=0;j<width && count < 1;j++){
                 Color c = new Color(image.getRGB(j, i));
                 int red = (int)(c.getRed());
                 int green = (int)(c.getGreen());
                 int blue = (int)(c.getBlue());
                 int total = red+green+blue;
                 
                 Color newColor;
                 if(total>thres){
                     newColor =  Color.WHITE;
                 }else{
                     newColor = Color.BLACK;
                     points.add(new Point(j,i));
                     count++;
                 }

                 white_image.setRGB(j,i,newColor.getRGB());
                 
             }
         }
        
        for(int i=0;i<height;i++){
            count=0;
             for(int j=width-1;j>=0 && count < 1;j--){
                 Color c = new Color(image.getRGB(j, i));
                 int red = (int)(c.getRed());
                 int green = (int)(c.getGreen());
                 int blue = (int)(c.getBlue());
                 int total = red+green+blue;
                 
                 Color newColor;
                 if(total>thres){
                     newColor =  Color.WHITE;
                 }else{
                     newColor = Color.BLACK;
                     points.add(new Point(j,i));
                     count++;
                 }
                 
                 white_image.setRGB(j,i,newColor.getRGB());
                 
             }
         }
        
        for(int j=0;j<width;j++){
            count=0;
             for(int i=0;i<height && count < 1;i++){
                 Color c = new Color(image.getRGB(j, i));
                 int red = (int)(c.getRed());
                 int green = (int)(c.getGreen());
                 int blue = (int)(c.getBlue());
                 int total = red+green+blue;
                 
                 Color newColor;
                 if(total>thres){
                     newColor =  Color.WHITE;
                 }else{
                     newColor = Color.BLACK;
                     points.add(new Point(j,i));
                     count++;
                 }
                 
                 white_image.setRGB(j,i,newColor.getRGB());
                 
             }
         }
        
        for(int j=0;j<width;j++){
            count=0;
             for(int i=height-1;i>=0 && count < 1;i--){
                 Color c = new Color(image.getRGB(j, i));
                 int red = (int)(c.getRed());
                 int green = (int)(c.getGreen());
                 int blue = (int)(c.getBlue());
                 int total = red+green+blue;
                 
                 Color newColor;
                 if(total>thres){
                     newColor =  Color.WHITE;
                 }else{
                     newColor = Color.BLACK;
                     points.add(new Point(j,i));
                     count++;
                 }
                 
                 white_image.setRGB(j,i,newColor.getRGB());
                 
             }
         }
        
        
        points.forEach( (p) -> {
            center.x += p.x;
            center.y += p.y;
        });  
        
        center.x /= points.size();
        center.y /= points.size();
        
        int pixel = 3;
        for(int a=(-pixel);a<pixel;a++){
            for(int b=(-pixel);b<pixel;b++){
                white_image.setRGB(center.x+a,center.y+b,Color.RED.getRGB());
            }
        }
        
        
        this.source = new File("C:\\Users\\JR\\Desktop\\findContour.jpg");
        this.image = white_image;
        File ouptut = this.source;
        ImageIO.write(white_image, "jpg", ouptut);        
    }
    
    public void findCorner()throws IOException{
      
        BufferedImage white_image = ImageIO.read(this.source);
        
        for(int i = 0;i<height;i++){
            for(int j = 0;j<width;j++){
                white_image.setRGB(j,i,image.getRGB(j,i));
            }
        }
         
        Collections.sort(points, (Point p1, Point p2) -> {
            double d1 = p1.distance(center);
            double d2 = p2.distance(center);
            return d1 > d2 ? -1 : (d1 == d2 ? 0 : 1);
        });
        
        List<Point> corner = new ArrayList<>();
        
        double far[] = new double[2];
        points.forEach((p) -> {
            if(corner.isEmpty()){
                corner.add(p);
                far[0] = p.distance(center);
            }else{
                boolean valid[] = new boolean[2];
                valid[0] = true;
                corner.forEach((q) -> {
                    if(q.distance(p) < far[0]){
                        valid[0] = false;
                    }
                });
                if(valid[0]){
                    corner.add(p);
                }
            }
        });
        
        int pixel = 3;
        for(int a=(-pixel);a<pixel;a++){
            for(int b=(-pixel);b<pixel;b++){
                white_image.setRGB(center.x+a,center.y+b,Color.RED.getRGB());
            }
        }
        
        corner.forEach((p) -> {
            for(int a=(-pixel);a<pixel;a++){
                for(int b=(-pixel);b<pixel;b++){
                    white_image.setRGB(p.x+a,p.y+b,Color.BLUE.getRGB());
                }
            }
        });       
        
        this.source = new File("C:\\Users\\JR\\Desktop\\findCorner.jpg");
        this.image = white_image;
        File ouptut = this.source;
        ImageIO.write(white_image, "jpg", ouptut);
        
        int num = corner.size();
        switch (num) {
            case 3:
                System.out.println("Triangle");
                shapeLabel.setText("Shape: Triangle");
                break;
            case 4:
                System.out.println("Square");
                shapeLabel.setText("Shape: Square");
                break;
            default:
                System.out.println("Circle");
                shapeLabel.setText("Shape: Circle");
                break;
        }
    }
}