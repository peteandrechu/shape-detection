class Pixel {
    constructor(x,y,orientation){
        this.x = x
        this.y = y
        this.orientation = orientation
    }
}

const startpixel = new Pixel(0,1,"north")
startpixel.x = 10
console.log(startpixel)
